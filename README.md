# TAM Stable branch: v1.2.5 (BROKEN USE UNSTABLE BRANCH)

A tampermonkey script which specifically focuses on skipping or removing midroll ads from twitch and any other ads by side effect by disabling them or quickly scrubbing through the entire ad.
#### It is currently unconfirmed if this affects ad revenue, as the ads are skipped and not forcibly unloaded/refused. 

<p>
<details>
<summary>Updates:</summary>

- {+Blacklists now added, add a streamers' channel name to the list to have the script not run on their channel.+}
- {+Added side effect: when the stream falls behind sometimes the script will scrub to the latest point.+}

</details>
</p>

To install, simply go [here](https://gitlab.com/8bit_shadow/twitch-anti-midroll/-/raw/master/twitch_anti-midroll.user.js). Once open, tampermonkey should automatically ask if you want to install the script. After you do you can then setup the variables as desired or leave them as default.
<br><br>
# TAM Unstable branch: v1.3.3 'destructive' (latest) / v1.0.0 'stream-switch' (most reliable & stable)

The [unstable branch](https://gitlab.com/8bit_shadow/twitch-anti-midroll/-/tree/unstable) has more options and will be updated more often then the main branch but will be broken far more often, if the main branch stops working at any point then try the unstable branch. The unstable branch is used for testing new features or trying to fix bugs with sometimes untested features/fixes.<br>
{-Make sure you disable the any other versions in tampermonkey if you want to. Running multiple will result in both undesirable and unpredictable events!-}
