// ==UserScript==
// @name         twitch anti-midroll
// @namespace    https://gitlab.com/8bit_shadow/twitch-anti-midroll
// @version      1.2.5
// @description  Automatically kills ads! Now without reloading!.
// @author       8Bit Shadow
// @match        https://www.twitch.tv/
// @downloadURL  https://gitlab.com/8bit_shadow/twitch-anti-midroll/-/raw/master/twitch_anti-midroll.user.js
// @updateURL    https://gitlab.com/8bit_shadow/twitch-anti-midroll/-/raw/master/twitch_anti-midroll.user.js
// @grant        none
// @run-at       document-start
// @include      /(^https:\/\/www\.twitch\.tv).*/
// ==/UserScript==

(function() {
    'use strict';

    //----------------------editable vars----------------------//
    let
    killAllAds = true,//set this to false if you only want midrolls to be disabled. Use blacklist to let all ads run.
        shouldAutoResolveTimeout = true,//if the script should try to auto resolve a loading timeout by pausing and playing the stream (forcing a resync).
        //Recommend keeping this active if you encounter a lot of random long loads.
        autoResolveTimeout = 5,//the amount of time the "auto-resolve timeout" script will wait for buffering in seconds before activating.
        //If you have a slow/unstable network, set this high (~10).
        hardAutoResolveTimout = 1500,//the maxiumum amount of time in miliseconds the video must run before autoResolveTimeout considers the recovery sucessful.
        //set this high for hard recovery to occur more often.
        checkInterval = 100,//how often, in miliseconds, the script will preform its checks.
        //Increase this to reduce the hit on your CPU, but expect more 'stuttering' when seeking. Reduce for script to take effect faster. This affects everything.
        blacklist = [
            "exmp",
            "exmp2"
        ];
    //Add channel names here to stop the script from running on that channel. This affects both mid and pre/post rolls.

    //----------------------editable vars----------------------//

    //vars
    let
    timer = 0,//if timer hits 1000 then 1 seconds have passed
        tr = true,//timer ran?
        runTimer = 0,//time the video has run uninterupted (up to hardAutoResolveTimout)
        vid = document.getElementsByTagName("video"),//gets all vidoes
        retrySkip = 0,//error out tracker.
        errored = false,//has errored out?
        failoutTimeout = 0,//falout's timeout, resets retrySkip to 0 at 2000
        retrySkipMax = 60,//maximum amount of failed retrys before the script stops trying to skip the pre/post roll
        previousTimeCheck = 0,//when = 3 previousTime gets checked
        previousTime = 0,//the time the video was at in the previous loop
        i = 0,//itterator for iframe[i]
        userData = [],//Array of all the objects we're looking for (V2 var)
        disabledAds = false;//have the ads been disabled by disableAds()? (V2 var)

    function sleep (time) {//from https://stackoverflow.com/a/951057
        return new Promise((resolve) => setTimeout(resolve, time));
    }

    function isNumeric(str) {//from https://stackoverflow.com/a/175787
        if (typeof str != "string") return false
        return !isNaN(str) &&
            !isNaN(parseFloat(str))
    }

    function resync()
    {//if the video gets stuck buffering a resync occurs
        if(vid[0].readyState == 1 && vid[0].currentTime != 0 && document.getElementsByClassName('tw-loading-spinner').length == 1 && shouldAutoResolveTimeout)
        {//if the video is buffering (readyState 1 means video paused, current time can only ever be 0 when buffering and there are only ever 1 element with that class unless an ad is running)
            if(runTimer < hardAutoResolveTimout && tr)
            {//and the video failed to run for at least x ms
                console.log("TAM: Preforming a hard resync.");
                vid[0].fastSeek(100);//hard stop
                sleep(100).then(() => {
                    vid[0].fastSeek(0);//force resync
                });
                tr = false;//reset trigger
            } else {
                if(timer == autoResolveTimeout*1000)
                {//and if the video buffered for x seconds (x*1000= miliseconds form)
                    console.log("TAM: Preforming a resync.");
                    vid[0].currentTime = 0;//reset current time
                    vid[0].fastSeek(0);//force resync
                    timer = 0;//reset timer
                    tr = true;//set trigger (recovery has ran)
                } else {
                    timer+= 100;//itterate that 100ms have passed while buffering
                }
            }
        }
        else if(tr && shouldAutoResolveTimeout)
        {//if the video is running after just recovering from a resync timeout (will not run if ad is running)
            if(runTimer >= hardAutoResolveTimout)
            {//and the amount of time ran uninterupted is not at max
                tr = false;//reset trigger
            } else {
                runTimer+= 100;//otherwise itterate that 100ms have passed uninterupted
            }
        }

        if(!tr && shouldAutoResolveTimeout)
        {//if video has not just recovered from a resync timeout
            if(failoutTimeout >= 4000 && vid[0].readyState == 3 && retrySkip != 0)
            {//if failout has not triggered, it's been 4 seconds, an ad is not playing and retrySkip has not already been reset
                retrySkip = 0;//reset failout
                failoutTimeout = 0;//reset failout resetter
            }
            else if (failoutTimeout < 4000)
            {
                failoutTimeout+= 100;
            }

            runTimer = 0;//reset run timer
        }

    }

    //V1 main
    function run()
    {
        if(document.URL.search("/videos/") != -1){return;}//if on video, dont run
        if(document.URL.search("/video/") != -1){return;}//if on video, dont run
        if(document.URL.search("/clip/") != -1){return;}//if on clip, dont run

        if(vid.length > 0)
        {//if there is at least one video
            if(vid[0].src != "")
            {//wait for video to load
                if(vid[0].readyState != 3 && retrySkip < retrySkipMax)
                {//if an ad is playing (ready state 4), or the video is paused (state 1) and retry count hasn't maxed out
                    if(previousTime == vid[0].currentTime)
                    {
                        previousTimeCheck++;
                    }
                    else
                    {
                        previousTimeCheck = 0;
                    }

                    if(previousTimeCheck == 10)
                    {//if the video is stuck (in case readState == 3 when it should be 1)
                        retrySkip = 0;
                        vid[0].playbackRate = 1073741824;
                        //                         vid[0].currentTime = 1073741824;//jump to the end of the ad (this is the maxiumum amount of time a video can be)
                        //                         vid[0].fastSeek(1073741824);//(which is 298,261h 37m 4s)
                        vid[0].play();
                        vid[0].pause();
                        vid[0].play();
                        console.log("TAM: Detected video stuck.");
                    }

                    if(vid[0].playbackRate != 10000)
                    {
                        vid[0].playbackRate = 1073741824;
                    }

                    if(vid[0].readyState == 1 && vid[0].currentTime != 0 && document.getElementsByClassName("tw-block tw-border-radius-medium tw-full-width tw-interactable tw-interactable--hover-enabled tw-interactable--overlay tw-interactive") != 1)
                    {//if the video is not paused, but is in a pause state; it's probably the ad which is fucked.
                        vid[0].play();
                        vid[0].pause();
                        vid[0].play();
                        console.log("TAM: Detected ad stuck.");
                    }

                    previousTime = vid[0].currentTime
                    retrySkip++;

                }
                else if (retrySkip >= retrySkipMax && !errored && (vid[0].readyState == 3 || vid[0].readyState == 4))
                {
                    console.clear();
                    console.log("TAM: Failed to skip Ad. If this occured when there was no ad then the channel may be bugged. Otherwise the ad was too long. Will retry in 4 seconds.");
                    console.log("TAM: If the streamers' channel is bugged, please turn off this script, add the channel to the blacklist and reload or switch killAllAds to false.");
                    errored = true;
                }

                resync();
            }
        }
    }

    //main
    function disableAds()
    {//attempts to disable the ads, given the userData exists.
        try
        {
            if(userData.length != 0)
            {
                console.log("TAM: Disableing ads...");
                let rootQuereyProperties = Object.keys(userData[0].data.ROOT_QUERY);
                if(rootQuereyProperties.length > 0)
                {
                    let userID = rootQuereyProperties.find((value) => { return value.startsWith("user({\"id\":\"")})
                    if(userID)
                    {
                        userID = userID.replace("user({\"id\":\"", "").replace("\"})" , "");//find the local userID
                    }
                    else{ return; }

                    if(isNumeric(userID.charAt(0)))
                    {//if the first character in the ID is a number
                        let adProperties = userData[0].data["$User:" + userID +".adProperties"];//get the adProperties we want to change
                        if(adProperties){
                            if(adProperties.hasPostrollsDisabled == false || adProperties.hasPrerollsDisabled == false
                               || adProperties.vodArchiveMidrolls != "" || adProperties.hasVodAdsEnabled == true)
                            {
                                adProperties.hasPostrollsDisabled = true;
                                adProperties.hasPrerollsDisabled = true;
                                adProperties.vodArchiveMidrolls = "";
                                adProperties.adServerDefault = "";
                                adProperties.hasVodAdsEnabled = false;
                                disabledAds = true;
                                console.log("TAM: Succesfully disabled midrolls! Enjoy a mid-roll free twitch.tv experience, without having to constantly reload!");
                            }
                        }
                        else
                        {
                            throw "adProperties is null! An error occured while trying to fetch the adProperties tied to your local userID.";
                        }
                    }
                    else
                    {
                        throw "userID is invalid! An error occured while trying to get your local userID!";
                    }
                }
                else
                {
                    throw "userData[0].data has no properties!";
                }
            }
            //             else
            //             {
            //                 console.log("TAM: DBG: userData.length is still 0.");
            //             }
        }
        catch(exception)
        {
            console.log("TAM: Critical error: Failed to disable ads, likely due to a reference error; " + exception.message + ".");
        }
    }

    //main-userData
    function getDataFromAdIframe()
    {//finds the iframe with the users' data by entering its internal instance and finding the data in the cache
        let iframe = document.getElementsByTagName("iframe");
        if(iframe.length > 0)
        {
            if(iframe[i].src.startsWith("https://aax-fe.amazon"))
            {
                let reacInternalInstance = Object.keys(iframe[i])[0];
                if(iframe[i][reacInternalInstance])
                {
                    if(iframe[i][reacInternalInstance].return)
                    {
                        if(iframe[i][reacInternalInstance].return.stateNode)
                        {
                            if(iframe[i][reacInternalInstance].return.stateNode.props)
                            {
                                if(iframe[i][reacInternalInstance].return.stateNode.props.connectAdIdentityResult)
                                {
                                    if(iframe[i][reacInternalInstance].return.stateNode.props.connectAdIdentityResult.client)
                                    {
                                        if(iframe[i][reacInternalInstance].return.stateNode.props.connectAdIdentityResult.client.cache)
                                        {
                                            if(iframe[i][reacInternalInstance].return.stateNode.props.connectAdIdentityResult.client.cache.data)
                                            {
                                                console.log("TAM: Got userData's data...");
                                                userData.push(iframe[i][reacInternalInstance].return.stateNode.props.connectAdIdentityResult.client.cache.data);
                                            }
                                            else
                                            {
                                                console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.return.stateNode.props.connectAdIdentityResult.client.cache.data");
                                            }
                                        }
                                        else
                                        {
                                            console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.return.stateNode.props.connectAdIdentityResult.client.cache");
                                        }
                                    }
                                    else
                                    {
                                        console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.return.stateNode.props.connectAdIdentityResult.client");
                                    }
                                }
                                else
                                {
                                    console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.return.stateNode.props.connectAdIdentityResult");
                                }
                            }
                            else
                            {
                                console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.return.stateNode.props");
                            }
                        }
                        else
                        {
                            console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.stateNode");
                        }
                    }
                    else
                    {
                        console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01.return");
                    }
                }
                else
                {
                    console.log("TAM: Failed to find iframe[" + i + "].__reactInternalInstance$xpr1756b01");
                }
            }
            else
            {
                if(i+1 < iframe.length)
                {
                    i++;
                }
            }
        }
    }

    //generic fix (stripped from V1)
    function playbackfix()
    {
        if(vid.length == 0)
        {
            vid = document.getElementsByTagName("video");
            if(vid.length == 0){ return; }
        }

        if(vid[0].playbackRate < 1 || vid[0].playbackRate > 1)
        {//keep playback rate syncronized (can sometimes be caused by network issues from reloading many times)
            vid[0].playbackRate = 1;
        }
    }

    function MainLoop()
    {//run the function to loop, then indefinetly loop after x ms (every x ms)
        if(document.URL.search("/videos/") != -1) { return; }//if video not loaded
        if(document.URL.search("/video/") != -1){return;}//if on video, dont run
        if(document.URL.search("/clip/") != -1){return;}//if on clip, dont run

        for(let i = 0; i < blacklist.length; i++)
        {

            if(document.URL.endsWith(blacklist[i]) && (blacklist[i] != "exmp" || blacklist[i] != "exmp2"))
            {
                return;
            }
        }

        playbackfix();

        if(userData.length == 0)
        {//if the userData has not been found yet
            getDataFromAdIframe();
        }

        if(!disabledAds)
        {//not disabled midrolls yet?
            disableAds();
        }

        if(killAllAds)
        {//disabling all ads?
            run();//run
        }

        sleep(checkInterval).then(() => {
            MainLoop();
        });
    }

    //running the code:
    console.log("TAM: Anti-midroll is running.");
    MainLoop();

})();
